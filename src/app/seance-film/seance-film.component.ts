import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CinemaService } from '../cinema.service';
import { FilmService } from '../film.service';
import { Cinema } from '../cinema';
import { Film } from '../film';
import { Seance } from '../seance';
import { SeanceService } from '../seance.service';
import { PanierService } from '../panier.service';

@Component({
  selector: 'app-seance-film',
  templateUrl: './seance-film.component.html',
  styleUrls: ['./seance-film.component.css']
})
export class SeanceFilmComponent implements OnInit {
  cinema : Cinema;
  films : Film;
  seances : Seance[];

  constructor(
    private route: ActivatedRoute,
    private cinemaService: CinemaService,
    private filmService: FilmService,
    private seanceService: SeanceService,
    private panierService: PanierService
  ) { }

  ngOnInit() {
    let idCinema : number = +this.route.snapshot.paramMap.get('idCinema');
    let idFilm : number = +this.route.snapshot.paramMap.get('idFilm');
    this.cinema = this.cinemaService.get(idCinema);
    this.films = this.filmService.get(idCinema, idFilm);
    this.seances = this.seanceService.getAllSeanceByCinemaByFilm(idCinema, idFilm);
  }

  ajouterAuPanier(seance) {
    alert('Ajout au panier');
    this.panierService.ajouterAuPanier(seance);
  }

}
