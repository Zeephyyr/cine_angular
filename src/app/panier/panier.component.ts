import { Component, OnInit } from '@angular/core';
import { PanierService } from '../panier.service';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.css']
})
export class PanierComponent implements OnInit {
  seances;

  constructor(
    private panierService : PanierService
  ) { }

  ngOnInit() {
    this.seances = this.panierService.getSeances();
    console.log("this.seances");
    console.log(this.seances);
  }

  retirerDuPanier(seance){
    this.seances = this.panierService.retirerDuPanier(seance);
  }
  viderPanier() {
    this.seances = this.panierService.viderPanier();
  }
}
