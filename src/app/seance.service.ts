import { Injectable } from '@angular/core';

import { seances } from './mock-seance-film-cinema';
import { Cinema } from './cinema';
import { Film } from './film';
import { Seance } from './seance';

@Injectable({
  providedIn: 'root'
})
export class SeanceService {

  constructor() { }
  /*
  getAll(idCinema, idFilm) : Seance[] {
    return seances.filter(function(seance) {
      return seance.film.cinemas.find(cinema => cinema.id === idCinema) !== undefined
    });
  }
  */
  getAllSeanceByCinemaByFilm(idCinema, idFilm): Seance[] {

    let seancesFiltered = seances.filter(
      s =>
        s.cinema.id === idCinema
    );

    return seancesFiltered = seancesFiltered.filter(
      s =>
        s.film.id === idFilm
    );
  }

}
