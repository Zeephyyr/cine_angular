import { Cinema } from './cinema';

export const cinemas : Cinema[] = [
    {
        id: 1,
        nom: 'Gaumont Valenciennes', 
        adresse: 'rue des Alpes 59300 Valenciennes',
        image: 'annu4912_1_c800x450.jpg'
    },
    {
        id: 2,
        nom: 'Cin\'Amand', 
        adresse: 'Rocade du Nord, 59230 Saint-Amand-les-Eaux',
        image: 'cinamand.png'
    },
    {
        id: 3,
        nom: 'L\'Imaginaire', 
        adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
        image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
    }
]