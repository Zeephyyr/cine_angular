import { Seance } from './seance';

export const seances : Seance[] = [
    //----- CINEMA 1 -----
    //Cinema 1 | Film 1 | Seance : March 01, 2020, 09:30:00 AM  
    {
        id : 1,
        cinema :
        {
            id: 1,
            nom: 'Gaumont Valenciennes', 
            adresse: 'rue des Alpes 59300 Valenciennes',
            image: 'annu4912_1_c800x450.jpg'
        },
        film:
        {
            id: 1,
            titre: 'D.A.R.Y.L.',
            duree: {hours: 1, minutes: 39},
            image: '19145239.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg',
            cinemas: [
                {
                    id: 1,
                    nom: 'Gaumont Valenciennes', 
                    adresse: 'rue des Alpes 59300 Valenciennes',
                    image: 'annu4912_1_c800x450.jpg'
                },
                {
                    id: 2,
                    nom: 'Cin\'Amand', 
                    adresse: 'Rocade du Nord, 59230 Saint-Amand-les-Eaux',
                    image: 'cinamand.png'
                },
                {
                    id: 3,
                    nom: 'L\'Imaginaire', 
                    adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
                    image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
                }
            ]
        },
        datetime : 'March 01, 2020, 09:30:00 AM',
        prix : 18
    },
    //Cinema 1 | Film 1 | Seance : March 01, 2020, 02:30:00 PM  
    {
        id : 2,
        cinema :
        {
            id: 1,
            nom: 'Gaumont Valenciennes', 
            adresse: 'rue des Alpes 59300 Valenciennes',
            image: 'annu4912_1_c800x450.jpg'
        },
        film:
        {
            id: 1,
            titre: 'D.A.R.Y.L.',
            duree: {hours: 1, minutes: 39},
            image: '19145239.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg',
            cinemas: [
                {
                    id: 1,
                    nom: 'Gaumont Valenciennes', 
                    adresse: 'rue des Alpes 59300 Valenciennes',
                    image: 'annu4912_1_c800x450.jpg'
                },
                {
                    id: 2,
                    nom: 'Cin\'Amand', 
                    adresse: 'Rocade du Nord, 59230 Saint-Amand-les-Eaux',
                    image: 'cinamand.png'
                },
                {
                    id: 3,
                    nom: 'L\'Imaginaire', 
                    adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
                    image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
                }
            ]
        },
        datetime : 'March 01, 2020, 02:30:00 PM',
        prix : 18
    },
    //Cinema 1 | Film 2 | Seance : March 01, 2020, 10:30:00 AM  
    {
        id : 3,
        cinema :
        {
            id: 1,
            nom: 'Gaumont Valenciennes', 
            adresse: 'rue des Alpes 59300 Valenciennes',
            image: 'annu4912_1_c800x450.jpg'
        },
        film:
        {
            id: 2,
            titre: 'USUAL SUSPECTS',
            duree: {hours: 1, minutes: 46},
            image: '69199504_af.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg',
            cinemas: [
                {
                    id: 1,
                    nom: 'Gaumont Valenciennes', 
                    adresse: 'rue des Alpes 59300 Valenciennes',
                    image: 'annu4912_1_c800x450.jpg'
                },
                {
                    id: 3,
                    nom: 'L\'Imaginaire', 
                    adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
                    image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
                }
            ]
        },
        datetime : 'March 01, 2020, 10:30:00 AM',
        prix : 18
    },
    //Cinema 1 | Film 2 | Seance : March 01, 2020, 10:00:00 PM  
    {
        id : 4,
        cinema :
        {
            id: 1,
            nom: 'Gaumont Valenciennes', 
            adresse: 'rue des Alpes 59300 Valenciennes',
            image: 'annu4912_1_c800x450.jpg'
        },
        film:
        {
            id: 2,
            titre: 'USUAL SUSPECTS',
            duree: {hours: 1, minutes: 46},
            image: '69199504_af.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg',
            cinemas: [
                {
                    id: 1,
                    nom: 'Gaumont Valenciennes', 
                    adresse: 'rue des Alpes 59300 Valenciennes',
                    image: 'annu4912_1_c800x450.jpg'
                },
                {
                    id: 3,
                    nom: 'L\'Imaginaire', 
                    adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
                    image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
                }
            ]
        },
        datetime : 'March 01, 2020, 10:00:00 PM',
        prix : 18
    },

    //----- CINEMA 2 -----
    //Cinema 2 | Film 1 | Seance : March 01, 2020, 09:30:00 AM  
    {
        id : 5,
        cinema :
        {
            id: 2,
            nom: 'Cin\'Amand', 
            adresse: 'Rocade du Nord, 59230 Saint-Amand-les-Eaux',
            image: 'cinamand.png'
        },
        film:
        {
            id: 1,
            titre: 'D.A.R.Y.L.',
            duree: {hours: 1, minutes: 39},
            image: '19145239.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg',
            cinemas: [
                {
                    id: 1,
                    nom: 'Gaumont Valenciennes', 
                    adresse: 'rue des Alpes 59300 Valenciennes',
                    image: 'annu4912_1_c800x450.jpg'
                },
                {
                    id: 2,
                    nom: 'Cin\'Amand', 
                    adresse: 'Rocade du Nord, 59230 Saint-Amand-les-Eaux',
                    image: 'cinamand.png'
                },
                {
                    id: 3,
                    nom: 'L\'Imaginaire', 
                    adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
                    image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
                }
            ]
        },
        datetime : 'March 01, 2020, 09:30:00 AM',
        prix : 18
    },
    //Cinema 2 | Film 1 | Seance : March 01, 2020, 02:30:00 PM  
    {
        id : 6,
        cinema :
        {
            id: 2,
            nom: 'Cin\'Amand', 
            adresse: 'Rocade du Nord, 59230 Saint-Amand-les-Eaux',
            image: 'cinamand.png'
        },
        film:
        {
            id: 1,
            titre: 'D.A.R.Y.L.',
            duree: {hours: 1, minutes: 39},
            image: '19145239.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg',
            cinemas: [
                {
                    id: 1,
                    nom: 'Gaumont Valenciennes', 
                    adresse: 'rue des Alpes 59300 Valenciennes',
                    image: 'annu4912_1_c800x450.jpg'
                },
                {
                    id: 2,
                    nom: 'Cin\'Amand', 
                    adresse: 'Rocade du Nord, 59230 Saint-Amand-les-Eaux',
                    image: 'cinamand.png'
                },
                {
                    id: 3,
                    nom: 'L\'Imaginaire', 
                    adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
                    image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
                }
            ]
        },
        datetime : 'March 01, 2020, 02:30:00 PM',
        prix : 18
    },
    //Cinema 2 | Film 3 | Seance : March 01, 2020, 10:00:00 AM  
    {
        id : 7,
        cinema :
        {
            id: 2,
            nom: 'Cin\'Amand', 
            adresse: 'Rocade du Nord, 59230 Saint-Amand-les-Eaux',
            image: 'cinamand.png'
        },
        film:
        {
            id: 3,
            titre: 'LA REINE DES NEIGES II',
            duree: {hours: 1, minutes: 44},
            image: '5952325.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg',
            cinemas: [
                {
                    id: 2,
                    nom: 'Cin\'Amand', 
                    adresse: 'Rocade du Nord, 59230 Saint-Amand-les-Eaux',
                    image: 'cinamand.png'
                },
                {
                    id: 3,
                    nom: 'L\'Imaginaire', 
                    adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
                    image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
                }
            ]
        },
        datetime : 'March 01, 2020, 10:00:00 AM',
        prix : 18
    },
    //Cinema 2 | Film 3 | Seance : March 01, 2020, 08:45:00 PM  
    {
        id : 8,
        cinema :
        {
            id: 2,
            nom: 'Cin\'Amand', 
            adresse: 'Rocade du Nord, 59230 Saint-Amand-les-Eaux',
            image: 'cinamand.png'
        },
        film:
        {
            id: 3,
            titre: 'LA REINE DES NEIGES II',
            duree: {hours: 1, minutes: 44},
            image: '5952325.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg',
            cinemas: [
                {
                    id: 2,
                    nom: 'Cin\'Amand', 
                    adresse: 'Rocade du Nord, 59230 Saint-Amand-les-Eaux',
                    image: 'cinamand.png'
                },
                {
                    id: 3,
                    nom: 'L\'Imaginaire', 
                    adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
                    image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
                }
            ]
        },
        datetime : 'March 01, 2020, 08:45:00 PM',
        prix : 18
    },

    //----- CINEMA 3 -----
    //Cinema 3 | Film 1 | Seance : March 01, 2020, 09:30:00 AM  
    {
        id : 9,
        cinema :
        {
            id: 3,
            nom: 'L\'Imaginaire', 
            adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
            image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
        },
        film:
        {
            id: 1,
            titre: 'D.A.R.Y.L.',
            duree: {hours: 1, minutes: 39},
            image: '19145239.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg',
            cinemas: [
                {
                    id: 1,
                    nom: 'Gaumont Valenciennes', 
                    adresse: 'rue des Alpes 59300 Valenciennes',
                    image: 'annu4912_1_c800x450.jpg'
                },
                {
                    id: 2,
                    nom: 'Cin\'Amand', 
                    adresse: 'Rocade du Nord, 59230 Saint-Amand-les-Eaux',
                    image: 'cinamand.png'
                },
                {
                    id: 3,
                    nom: 'L\'Imaginaire', 
                    adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
                    image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
                }
            ]
        },
        datetime : 'March 01, 2020, 09:30:00 AM',
        prix : 18
    },
    //Cinema 3 | Film 1 | Seance : March 01, 2020, 02:30:00 PM  
    {
        id : 10,
        cinema :
        {
            id: 3,
            nom: 'L\'Imaginaire', 
            adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
            image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
        },
        film:
        {
            id: 1,
            titre: 'D.A.R.Y.L.',
            duree: {hours: 1, minutes: 39},
            image: '19145239.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg',
            cinemas: [
                {
                    id: 1,
                    nom: 'Gaumont Valenciennes', 
                    adresse: 'rue des Alpes 59300 Valenciennes',
                    image: 'annu4912_1_c800x450.jpg'
                },
                {
                    id: 2,
                    nom: 'Cin\'Amand', 
                    adresse: 'Rocade du Nord, 59230 Saint-Amand-les-Eaux',
                    image: 'cinamand.png'
                },
                {
                    id: 3,
                    nom: 'L\'Imaginaire', 
                    adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
                    image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
                }
            ]
        },
        datetime : 'March 01, 2020, 02:30:00 PM',
        prix : 18
    },
    //Cinema 3 | Film 2 | Seance : March 01, 2020, 10:30:00 AM  
    {
        id : 11,
        cinema :
        {
            id: 3,
            nom: 'L\'Imaginaire', 
            adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
            image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
        },
        film:
        {
            id: 2,
            titre: 'USUAL SUSPECTS',
            duree: {hours: 1, minutes: 46},
            image: '69199504_af.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg',
            cinemas: [
                {
                    id: 1,
                    nom: 'Gaumont Valenciennes', 
                    adresse: 'rue des Alpes 59300 Valenciennes',
                    image: 'annu4912_1_c800x450.jpg'
                },
                {
                    id: 3,
                    nom: 'L\'Imaginaire', 
                    adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
                    image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
                }
            ]
        },
        datetime : 'March 01, 2020, 10:30:00 AM',
        prix : 18
    },
    //Cinema 3 | Film 2 | Seance : March 01, 2020, 10:00:00 PM  
    {
        id : 12,
        cinema :
        {
            id: 3,
            nom: 'L\'Imaginaire', 
            adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
            image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
        },
        film:
        {
            id: 2,
            titre: 'USUAL SUSPECTS',
            duree: {hours: 1, minutes: 46},
            image: '69199504_af.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg',
            cinemas: [
                {
                    id: 1,
                    nom: 'Gaumont Valenciennes', 
                    adresse: 'rue des Alpes 59300 Valenciennes',
                    image: 'annu4912_1_c800x450.jpg'
                },
                {
                    id: 3,
                    nom: 'L\'Imaginaire', 
                    adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
                    image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
                }
            ]
        },
        datetime : 'March 01, 2020, 10:00:00 PM',
        prix : 18
    },
    //Cinema 3 | Film 3 | Seance : March 01, 2020, 10:00:00 AM  
    {
        id : 13,
        cinema :
        {
            id: 3,
            nom: 'L\'Imaginaire', 
            adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
            image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
        },
        film:
        {
            id: 3,
            titre: 'LA REINE DES NEIGES II',
            duree: {hours: 1, minutes: 44},
            image: '5952325.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg',
            cinemas: [
                {
                    id: 2,
                    nom: 'Cin\'Amand', 
                    adresse: 'Rocade du Nord, 59230 Saint-Amand-les-Eaux',
                    image: 'cinamand.png'
                },
                {
                    id: 3,
                    nom: 'L\'Imaginaire', 
                    adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
                    image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
                }
            ]
        },
        datetime : 'March 01, 2020, 10:00:00 AM',
        prix : 18
    },
    //Cinema 3 | Film 3 | Seance : March 01, 2020, 08:45:00 PM  
    {
        id : 14,
        cinema :
        {
            id: 3,
            nom: 'L\'Imaginaire', 
            adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
            image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
        },
        film:
        {
            id: 3,
            titre: 'LA REINE DES NEIGES II',
            duree: {hours: 1, minutes: 44},
            image: '5952325.jpg-c_215_290_x-f_jpg-q_x-xxyxx.jpg',
            cinemas: [
                {
                    id: 2,
                    nom: 'Cin\'Amand', 
                    adresse: 'Rocade du Nord, 59230 Saint-Amand-les-Eaux',
                    image: 'cinamand.png'
                },
                {
                    id: 3,
                    nom: 'L\'Imaginaire', 
                    adresse: 'Place Paul Eluard, 59282 Douchy-les-Mines',
                    image: 'B9717077830Z.1_20180928174605_000+G58C4H90P.2-0.jpg'
                }
            ]
        },
        datetime : 'March 01, 2020, 08:45:00 PM',
        prix : 18
    },
]