import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PanierService {
  seances = [];

  constructor() { }

  ajouterAuPanier(seance) {
    this.seances.push(seance);
    console.log(this.seances);
  }
  
  retirerDuPanier(seance) {
    var index = this.seances.indexOf(seance);
    console.log(index);
    this.seances.splice(index, 1);
    return this.seances;
  }

  getSeances() {
    return this.seances;
  }

  viderPanier() {
    return this.seances = [];
  }
}
